#!/usr/bin/env nextflow

process scnanogps_read_length_profiler {
// Runs scNanoGPS read length profiler
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(fq) - FASTQ
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: alns
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*am') - Alignment File

// require:
//   FQS
//   IDX_FILES
//   params.star$star_map_parameters
//   params.star$star_alt_capture

  tag "${dataset}/${pat_name}/${run}"
  label 'scnanogps_container'
  label 'scnanogps_read_length_profiler'
  publishDir "${params.qc_out_dir}/${dataset}/${pat_name}/${run}/scnanogps_read_length_profiler"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("${dataset}-${pat_name}-${run}*am"), emit: alns

  script:
  """
  python3 /scNanoGPS/other_utils/read_length_profiler.py \
    -i ${fq} \
    $parstr
  """
}

process scnanogps_prepare_read_qc {
// Runs scNanoGPS prepare read QC
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(fq) - FASTQ
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: alns
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*am') - Alignment File

// require:
//   FQS
//   IDX_FILES
//   params.star$star_map_parameters
//   params.star$star_alt_capture

  tag "${dataset}/${pat_name}/${run}"
  label 'scnanogps_container'
  label 'scnanogps_prepare_read_qc'
  publishDir "${params.qc_out_dir}/${dataset}/${pat_name}/${run}/scnanogps_prepare_read_qc"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("${dataset}-${pat_name}-${run}*am"), emit: alns

  script:
  """
  python3 /scNanoGPS/other_utils/prepare_read_qc.py \
    -i ${fq} \
    $parstr
  """
}


process scnanogps_scanner {
// Runs scNanoGPS scanner
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(fq) - FASTQ
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: alns
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*am') - Alignment File

// require:
//   FQS
//   IDX_FILES
//   params.star$star_map_parameters
//   params.star$star_alt_capture

  tag "${dataset}/${pat_name}/${run}"
  label 'scnanogps_container'
  label 'scnanogps_scanner'
  publishDir "${params.qc_out_dir}/${dataset}/${pat_name}/${run}/scnanogps_scanner"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("barcode_list.tsv.gz"), emit: barcode_lists

  script:
  """
  python3 /scNanoGPS/scanner.py \
    -i ${fq} \
    -t ${task.cpus} \
    $parstr
  """
}


process scnanogps_assigner {
// Runs scNanoGPS assigner
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(fq) - FASTQ
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: alns
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*am') - Alignment File

// require:
//   FQS
//   IDX_FILES
//   params.star$star_map_parameters
//   params.star$star_alt_capture

  tag "${dataset}/${pat_name}/${run}"
  label 'scnanogps_container'
  label 'scnanogps_assigner'
  publishDir "${params.qc_out_dir}/${dataset}/${pat_name}/${run}/scnanogps_assigner"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(cb_list)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("${dataset}-${pat_name}-${run}*am"), emit: alns

  script:
  """
  python3 /scNanoGPS/assigner.py \
    -i ${cb_list} \
    -t ${task.cpus} \
    -parstr
  """
}

process scnanogps_curator {
// Runs scNanoGPS curator
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(fq) - FASTQ
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: alns
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*am') - Alignment File

// require:
//   FQS
//   IDX_FILES
//   params.star$star_map_parameters
//   params.star$star_alt_capture

  tag "${dataset}/${pat_name}/${run}"
  label 'scnanogps_container'
  label 'scnanogps_curator'
  publishDir "${params.qc_out_dir}/${dataset}/${pat_name}/${run}/scnanogps_curator"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(cb_list)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("${dataset}-${pat_name}-${run}*am"), emit: alns

  script:
  """
  python3 /scNanoGPS/curator.py \
    -i ${cb_list} \
    -t ${task.cpus} \
    -parstr
  """
}

// Need scnanogps_reporter
